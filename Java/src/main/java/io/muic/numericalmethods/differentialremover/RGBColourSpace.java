package io.muic.numericalmethods.differentialremover;

public class RGBColourSpace {
    private final int red;
    private final int blue;
    private final int green;
    private final int alpha;

    public RGBColourSpace(int red, int blue, int green, int alpha) {
        this.red = red;
        this.blue = blue;
        this.green = green;
        this.alpha = alpha;
    }

    public int sum(){
        return red + blue + green;
    }

    public int getRed() {
        return red;
    }

    public int getBlue() {
        return blue;
    }

    public int getGreen() {
        return green;
    }

    public int getAlpha() {
        return alpha;
    }
}
