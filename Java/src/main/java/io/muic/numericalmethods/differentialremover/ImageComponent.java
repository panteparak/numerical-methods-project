package io.muic.numericalmethods.differentialremover;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;

public class ImageComponent {
    private Path path;
    private BufferedImage image;

    public ImageComponent(Path path) throws IOException {
        this.path = path;
        image = ImageIO.read(new BufferedInputStream(new FileInputStream(path.toFile())));
    }

    public Color getPixelAt(int i, int j) {
        int pixel = image.getRGB(i, j);
        return new Color(pixel);
    }

    public void setPixelAt(int i, int j, Color color){
        image.setRGB(i,j, color.getRGB());
    }
}
