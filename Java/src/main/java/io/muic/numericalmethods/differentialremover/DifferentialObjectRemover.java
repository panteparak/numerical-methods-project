package io.muic.numericalmethods.differentialremover;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DifferentialObjectRemover {
    private ExecutorService executor = null;
    private Path path;
    private File output;

    public DifferentialObjectRemover(Path path, File output) {
        this.path = path;
        this.output = output;
    }

    public void start() throws IOException, NoSuchFileException {
        DirectoryWalker directoryWalker = new DirectoryWalker(path);
        List<Path> image_paths = directoryWalker.getImages();

        if (image_paths.size() == 0){
            throw new NoSuchFileException("Image file not found");
        }

        List<ImageComponent> images = new ArrayList<>();

        for (Path path : image_paths)
            images.add(new ImageComponent(path));

        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }
}
