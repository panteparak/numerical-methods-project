package io.muic.numericalmethods.differentialremover;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.apache.tika.Tika;
import org.apache.tika.detect.Detector;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DirectoryWalker {
    private final Path location;

    public DirectoryWalker(Path path) {
        location = path;
    }

    public List<Path> getImages() throws IOException{

        return Collections.unmodifiableList(
                Files
                        .walk(location)
                        .parallel()
                        .filter(path -> path.toFile().isFile())
                        .filter(path -> {
                            try {
                                return new Tika().detect(path).startsWith("image/");
                            } catch (IOException e) {}
                            return false;
                        }).collect(Collectors.toList()));
    }

    public static void main(String[] args) throws IOException{
        DirectoryWalker directoryWalker = new DirectoryWalker(Paths.get("/","Users", "panteparak","Workspace/MUIC/Year 4 - Term 1/Numerical Methods/Project/Python/imgs"));
        System.out.println(directoryWalker.getImages());
    }
}
